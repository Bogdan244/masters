//
//  String+Helper.swift
//  Masters
//
//  Created by admin on 10/17/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    static func createText(boldText: String, nonBoldText: String, color: UIColor = .black) -> NSAttributedString {
        let boldAttr = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 15)]
        let defaultAttr = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15),
                           NSAttributedString.Key.foregroundColor: color]
        let attrString = NSMutableAttributedString(string: boldText, attributes: boldAttr)
        let attrString2 = NSMutableAttributedString(string: nonBoldText, attributes: defaultAttr)
        attrString.append(attrString2)
        return attrString
    }
    
    static func createTextIfExist(boldText: String, nonBoldText: String, color: UIColor = .black) -> NSAttributedString {
        if !boldText.isEmpty && !nonBoldText.isEmpty {
            return String.createText(boldText: boldText, nonBoldText: nonBoldText, color: color)
        } else {
            return NSAttributedString(string: "")
        }
    }
}

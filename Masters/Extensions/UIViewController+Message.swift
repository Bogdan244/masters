//
//  UIViewController+Message.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlertWithMessage(title: String?, message: String, complition: (() -> Void)?) {
        var str = message
        if message == "Success" {
            str = "Готово"
        } else if message == "Validation error" {
            str = "Введите комментарий и время"
        }
        let alert = UIAlertController(title: title, message: str, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
            complition?()
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func showAlertMessage(title: String?, message: String) {
        showAlertWithMessage(title: title, message: message, complition: nil)
    }
    
    func showError() {
        showAlertMessage(title: "Ошибка", message: "Неизвестная ошибка")
    }
}

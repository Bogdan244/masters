//
//  ViewController+Loader.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

extension UIViewController: NVActivityIndicatorViewable {
    
    func showLoader() {
        let activity = ActivityData()
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activity, nil)
    }
    
    func hideLoader() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
}

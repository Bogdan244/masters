//
//  Button+Helper.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

extension UIButton {
    func addCornerLine() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.borderColor = self.titleLabel?.textColor.cgColor
    }
}

extension UIView {
    func addCorner() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.black.cgColor
    }
}

extension UILabel {
    func addCornerLine() {
        self.layer.cornerRadius = 5
        self.layer.borderWidth = 1
        self.layer.borderColor = textColor.cgColor
    }
}

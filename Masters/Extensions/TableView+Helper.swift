//
//  TableView+Helper.swift
//  Masters
//
//  Created by admin on 10/17/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {

    func addEmptyView(text: String) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: bounds.size.width - 60, height: bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = text
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 20)
      //  messageLabel.sizeToFit()
        let view = UIView()
        backgroundView = view
        view.addSubview(messageLabel)
        messageLabel.center = view.center
        separatorStyle = .none
    }
    
    func removeEmptyView() {
        backgroundView = nil
    }
}

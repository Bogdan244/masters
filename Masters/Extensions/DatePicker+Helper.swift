//
//  DatePicker+Color.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import UIKit

extension UIDatePicker {
    
    func setTextColor(color: UIColor) {
        self.setValue(color, forKeyPath: "textColor")
    }
}

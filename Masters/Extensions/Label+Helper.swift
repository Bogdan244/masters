//
//  Label+Helper.swift
//  Masters
//
//  Created by admin on 11/5/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func addLongTapCall() {
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(call))
        gesture.minimumPressDuration = 0.5
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(gesture)
    }
    
    @objc func call(sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            let callString = self.text?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined() ?? ""
            guard let phone = Int(callString), let url = URL(string: "tel://" + "\(phone)") else {
                UIApplication.topViewController()?.showAlertMessage(title: "Ошибка", message: "Некорректный номер")
                return
            }
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        default:
            return
        }
        
    }
    
}

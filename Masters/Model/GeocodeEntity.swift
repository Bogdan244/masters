//
//  GeocodeEntity.swift
//  Masters
//
//  Created by admin on 11/6/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

class GeocodeEntity: Codable {
    let results: [Result]
    let status: String
    
    enum CodingKeys: String, CodingKey {
        case results = "results"
        case status = "status"
    }
    
    init(results: [Result], status: String) {
        self.results = results
        self.status = status
    }
}

class Result: Codable {
    let addressComponents: [AddressComponent]
    let formattedAddress: String
    let geometry: Geometry
    let placeID: String
    let types: [String]
    
    enum CodingKeys: String, CodingKey {
        case addressComponents = "address_components"
        case formattedAddress = "formatted_address"
        case geometry = "geometry"
        case placeID = "place_id"
        case types = "types"
    }
    
    init(addressComponents: [AddressComponent], formattedAddress: String, geometry: Geometry, placeID: String, types: [String]) {
        self.addressComponents = addressComponents
        self.formattedAddress = formattedAddress
        self.geometry = geometry
        self.placeID = placeID
        self.types = types
    }
}

class AddressComponent: Codable {
    let longName: String
    let shortName: String
    let types: [String]
    
    enum CodingKeys: String, CodingKey {
        case longName = "long_name"
        case shortName = "short_name"
        case types = "types"
    }
    
    init(longName: String, shortName: String, types: [String]) {
        self.longName = longName
        self.shortName = shortName
        self.types = types
    }
}

class Geometry: Codable {
    let location: Location
    let locationType: String
    let viewport: Viewport
    
    enum CodingKeys: String, CodingKey {
        case location = "location"
        case locationType = "location_type"
        case viewport = "viewport"
    }
    
    init(location: Location, locationType: String, viewport: Viewport) {
        self.location = location
        self.locationType = locationType
        self.viewport = viewport
    }
}

class Location: Codable {
    let lat: Double
    let lng: Double
    
    enum CodingKeys: String, CodingKey {
        case lat = "lat"
        case lng = "lng"
    }
    
    init(lat: Double, lng: Double) {
        self.lat = lat
        self.lng = lng
    }
}

class Viewport: Codable {
    let northeast: Location
    let southwest: Location
    
    enum CodingKeys: String, CodingKey {
        case northeast = "northeast"
        case southwest = "southwest"
    }
    
    init(northeast: Location, southwest: Location) {
        self.northeast = northeast
        self.southwest = southwest
    }
}

//
//  User.swift
//  Masters
//
//  Created by admin on 10/17/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

@objcMembers class User: Object, Decodable {
    dynamic var userKey = Constants.userKey
    dynamic var email: String = ""
    dynamic var id: Int = 0
    dynamic var phone: String = ""
    dynamic var language: String = ""
    dynamic var name: String = ""
    dynamic var accessToken: String = ""
    
    var token: String {
        return "Bearer " + accessToken
    }
    
    enum CodingKeys: String, CodingKey {
        case email
        case id
        case phone
        case languague
        case name
        case accessToken = "access_token"
    }
    
    enum DataKeys: String, CodingKey {
        case data
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: DataKeys.self).nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        email = try container.decodeIfPresent(String.self, forKey: .email) ?? ""
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        phone = try container.decodeIfPresent(String.self, forKey: .phone) ?? ""
        language = try container.decodeIfPresent(String.self, forKey: .languague) ?? ""
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        accessToken = try container.decodeIfPresent(String.self, forKey: .accessToken) ?? ""
    }
    
    override class func primaryKey() -> String? {
        return "userKey"
    }
    
    static var current: User? {
        do {
            let realm = try Realm()
            let selfEntity = realm.object(ofType: User.self, forPrimaryKey: Constants.userKey)
            return selfEntity
        } catch {
            return nil
        }
    }
    
}


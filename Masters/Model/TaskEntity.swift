//
//  Task.swift
//  Masters
//
//  Created by admin on 10/17/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

class TasksEntity: Decodable {
    let currentPage: Int
    let lastPage: Int
    let display: Int
    var tasks: [TaskEntity]
    let total: Int
    let statusesCount: StatusesCount
    
    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case lastPage = "last_page"
        case display
        case tasks
        case total
        case statusesCount = "status_count"
    }
    
    enum DataKeys: String, CodingKey {
        case data
    }
    
    init(currentPage: Int, lastPage: Int, display: Int, tasks: [TaskEntity], total: Int, statusesCount: StatusesCount) {
        self.currentPage = currentPage
        self.lastPage = lastPage
        self.display = display
        self.tasks = tasks
        self.total = total
        self.statusesCount = statusesCount
    }
    
    required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DataKeys.self).nestedContainer(keyedBy: CodingKeys.self, forKey: .data)
        let currentPage = try container.decode(Int.self, forKey: .currentPage)
        let lastPage = try container.decode(Int.self, forKey: .lastPage)
        let display = try container.decode(Int.self, forKey: .display)
        let tasks = try container.decode([TaskEntity].self, forKey: .tasks)
        let total = try container.decode(Int.self, forKey: .total)
        let statusesCount = try container.decode(StatusesCount.self, forKey: .statusesCount)
        
        self.init(currentPage: currentPage, lastPage: lastPage, display: display, tasks: tasks, total: total, statusesCount: statusesCount)
    }
    
}

class TaskEntity: Decodable {
    var status: Status = Status.undefined
    var id: Int = 0
    var clientDevice: String = ""
    var clientAddress: String = ""
    var clientPhone: String = ""
    var clientFio: String = ""
    var unit: String = ""
    var type: String = ""
    var priority: String = ""
    var taskCode: String = ""
    var refusedComment: String = ""
    var desiredClientTime: String = ""
    var agreedWorkingTime: String? = nil
    var workingTime: String? = nil
    
    enum CodingKeys: String, CodingKey {
        case status
        case id
        case clientDevice = "client_device"
        case clientAddress = "client_address"
        case clientPhone = "client_phone"
        case clientFio = "client_fio"
        case unit
        case refusedComment = "refused_comment"
        case type
        case priority
        case taskCode = "task_code"
        case desiredClientTime = "desired_client_time"
        case agreedWorkingTime = "agreed_working_time"
        case workingTime = "working_time"
    }
    
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try container.decodeIfPresent(Status.self, forKey: .status) ?? Status.undefined
        id =  try container.decode(Int.self, forKey: .id)
        clientDevice = try container.decodeIfPresent(String.self, forKey: .clientDevice) ?? ""
        clientAddress = try container.decodeIfPresent(String.self, forKey: .clientAddress) ?? ""
        clientPhone = try container.decodeIfPresent(String.self, forKey: .clientPhone) ?? ""
        clientFio = try container.decodeIfPresent(String.self, forKey: .clientFio) ?? ""
        unit = try container.decodeIfPresent(String.self, forKey: .unit) ?? ""
        refusedComment = try container.decodeIfPresent(String.self, forKey: .refusedComment) ?? ""
        type = try container.decodeIfPresent(String.self, forKey: .type) ?? ""
        priority = try container.decodeIfPresent(String.self, forKey: .priority) ?? ""
        taskCode = try container.decodeIfPresent(String.self, forKey: .taskCode) ?? ""
        desiredClientTime = try container.decodeIfPresent(String.self, forKey: .desiredClientTime) ?? ""
        agreedWorkingTime = try container.decodeIfPresent(String.self, forKey: .agreedWorkingTime) 
        workingTime = try container.decodeIfPresent(String.self, forKey: .workingTime) 
        
    }
    
}

struct StatusesCount: Decodable {
    var accepted = 0
    var closed = 0
    var open = 0
    var refused = 0
    var working = 0
}

public enum Status: String, Codable {
    case accepted = "accepted"
    case open = "open"
    case working = "working"
    case refused = "refused"
    case closed = "closed"
    case undefined = "undefined"
    case openAndRefused = ""
}

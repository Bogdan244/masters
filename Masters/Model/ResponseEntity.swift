//
//  ResponseEntity.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

class ResponseEntity: Decodable {
    let message: String
    let errorEntity: [ErrorEntity]
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case errorEntity = "errors"
    }
    
    init(message: String, errors: [ErrorEntity]) {
        self.message = message
        self.errorEntity = errors
    }
}

class ErrorEntity: Decodable {
    let message: String
    let data: String
    let code: Int
    
    enum CodingKeys: String, CodingKey {
        case message = "message"
        case data = "data"
        case code = "code"
    }
    
    init(message: String, data: String, code: Int) {
        self.message = message
        self.data = data
        self.code = code
    }
}

//
//  TaskCell.swift
//  Masters
//
//  Created by admin on 10/17/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

protocol TaskCellDelegate: class {
    func didSelectButton(cell: TaskCell)
    func didSelectLocationButton(cell: TaskCell)
}

class TaskCell: UITableViewCell {
    
    weak var delegate: TaskCellDelegate?
    
    @IBOutlet weak var taskNumber: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var taskType: UILabel!
    @IBOutlet weak var desiredDate: UILabel!
    @IBOutlet weak var refusedComment: UILabel!
    @IBOutlet weak var unit: UILabel!
    @IBOutlet weak var refusedStatusAlert: UILabel!
    @IBOutlet weak var takeToWorkBt: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        takeToWorkBt.addCornerLine()
        refusedStatusAlert.addCornerLine()
    }

    func configurate(with task: TaskEntity) {
        taskNumber.attributedText = String.createText(boldText: "Номер заявки: ", nonBoldText: task.taskCode)
        address.attributedText = String.createText(boldText: "Адрес: ", nonBoldText: task.clientAddress)
        taskType.attributedText = String.createText(boldText: "Вид заявки: ", nonBoldText: task.type)
        desiredDate.attributedText = String.createText(boldText: "Желаемая дата: ", nonBoldText: task.desiredClientTime)
        refusedComment.attributedText = String.createTextIfExist(boldText: "Комментарий отказа: ", nonBoldText: task.refusedComment)
        unit.attributedText = String.createTextIfExist(boldText: "Магазин: ", nonBoldText: task.unit)
        refusedStatusAlert.isHidden = task.status != .refused
    }
    
    @IBAction func selectButton(_ sender: Any) {
        delegate?.didSelectButton(cell: self)
    }
    
    @IBAction func locationButton(_ sender: Any) {
        delegate?.didSelectLocationButton(cell: self)
    }
}

//
//  CopyTextButton.swift
//  Smart24_IOS_redesign
//
//  Created by Bodya on 30.05.2018.
//  Copyright © 2018 Vitya. All rights reserved.
//

import UIKit

class CopyTextButton: UIButton {
    
    var copyText: String = ""
    
    func setCopyText(text: String, for controlEvants: UIControl.Event) {
        copyText = text
        addTarget(self, action: #selector(copyTextAction), for: controlEvants)
    }
    
    @objc private func copyTextAction() {
        UIPasteboard.general.string = copyText
        showAlert()
    }
    
    private func showAlert() {
        guard let vc = UIApplication.topViewController() else { return }
        let label = alertViewForVC(vc: vc)
        vc.view.addSubview(label)
        
        UIView.animate(withDuration: 0.5, animations: {
            label.alpha = 1
        }) { (_) in
            UIView.animate(withDuration: 1, animations: {
                label.alpha = 0
            }, completion: { (_) in
                label.removeFromSuperview()
            })
        }
        
    }
    
    private func alertViewForVC(vc: UIViewController) -> UIView {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        label.text = "Текст добавлен в буфер"
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0
        label.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.77)
        label.clipsToBounds = true
        label.alpha = 0
        label.sizeToFit()
        label.layer.cornerRadius = 5
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.black.cgColor
        label.bounds.size = CGSize(width: label.bounds.size.width + 20, height: label.bounds.size.height + 10)
        label.center = vc.view.center
        
        return label
    }

}

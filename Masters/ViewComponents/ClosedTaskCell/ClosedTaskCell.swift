//
//  ClosedTaskCell.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

protocol ClosedTaskCellDelegate: class {
    func didSelectLocationButton(cell: ClosedTaskCell)
}

class ClosedTaskCell: UITableViewCell {

    weak var delegate: ClosedTaskCellDelegate?
    
    @IBOutlet weak var taskNumber: UILabel!
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var taskType: UILabel!
    @IBOutlet weak var desiredDate: UILabel!
    @IBOutlet weak var unit: UILabel!
    @IBOutlet weak var workingDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        workingDate.layer.cornerRadius = 5
        workingDate.layer.masksToBounds = true
    }

    func configurate(with task:TaskEntity) {
        
        taskNumber.attributedText = String.createText(boldText: "Номер заявки: ", nonBoldText: task.taskCode)
        address.attributedText = String.createText(boldText: "Адрес: ", nonBoldText: task.clientAddress)
        
        taskType.attributedText = String.createText(boldText: "Вид заявки: ", nonBoldText: task.type)
        desiredDate.attributedText = String.createText(boldText: "Желаемая дата: ", nonBoldText: task.desiredClientTime)
        unit.attributedText = String.createTextIfExist(boldText: "Магазин: ", nonBoldText: task.unit)
        var str = "Выполнено"
        if let workingTime = task.workingTime {
            str = str + " " + workingTime
        }
        workingDate.text = str
    }
 
    @IBAction func selectLocationButton(_ sender: Any) {
        delegate?.didSelectLocationButton(cell: self)
    }
    
    
}

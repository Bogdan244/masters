//
//  DepartureTaskCell.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

protocol DepartureTaskCellDelegate: class {
    func didSelectAcceptTaskButton(cell: DepartureTaskCell)
    func didSelectRemoveTaskButton(cell: DepartureTaskCell)
    func didSelectLocationButton(cell: DepartureTaskCell)
}

class DepartureTaskCell: UITableViewCell {

    weak var delegate: DepartureTaskCellDelegate?
    
    @IBOutlet weak var taskNumber: UILabel!
    @IBOutlet weak var dateDepartureLb: UILabel!
    @IBOutlet weak var timeDepartureLb: UILabel!
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var taskType: UILabel!
    @IBOutlet weak var desiredDate: UILabel!
    @IBOutlet weak var clientNameLb: UILabel!
    @IBOutlet weak var clientContactNumber: UILabel!
    @IBOutlet weak var unitLb: UILabel!
    @IBOutlet weak var refusedCommentLb: UILabel!
    
    @IBOutlet weak var removeTaskBT: UIButton!
    @IBOutlet weak var acceptTaskBT: UIButton!
    @IBOutlet weak var copyContactNumberBT: CopyTextButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        removeTaskBT.addCornerLine()
        acceptTaskBT.addCornerLine()
        clientContactNumber.addLongTapCall()
    }

    func configurate(with task:TaskEntity) {
        if let dateString = task.agreedWorkingTime {
            let dateArray = dateString.components(separatedBy: " ")
            dateDepartureLb.attributedText = String.createText(boldText: "Дата выезда: ", nonBoldText: dateArray.first!)
            timeDepartureLb.attributedText = String.createText(boldText: "Время визита: ", nonBoldText: dateArray.last!)
        }
        taskNumber.attributedText = String.createText(boldText: "Номер заявки: ", nonBoldText: task.taskCode)
        address.attributedText = String.createText(boldText: "Адрес: ", nonBoldText: task.clientAddress)
        
        taskType.attributedText = String.createText(boldText: "Вид заявки: ", nonBoldText: task.type)
        desiredDate.attributedText = String.createText(boldText: "Желаемая дата: ", nonBoldText: task.desiredClientTime)
        clientNameLb.attributedText = String.createText(boldText: "ФИО клиента: ", nonBoldText: task.clientFio)
        clientContactNumber.attributedText = String.createText(boldText: "Контактный номер: ", nonBoldText: task.clientPhone)
        unitLb.attributedText = String.createTextIfExist(boldText: "Магазин: ", nonBoldText: task.unit)
        refusedCommentLb.attributedText = String.createTextIfExist(boldText: "Комментарий отказа: ", nonBoldText: task.refusedComment)
        copyContactNumberBT.setCopyText(text: task.clientPhone, for: UIControl.Event.touchUpInside)
    }
    
    @IBAction func removeTaskAction(_ sender: Any) {
        delegate?.didSelectRemoveTaskButton(cell: self)
    }
    
    @IBAction func acceptTaskAction(_ sender: Any) {
        delegate?.didSelectAcceptTaskButton(cell: self)
    }
    
    @IBAction func locationAction(_ sender: Any) {
        delegate?.didSelectLocationButton(cell: self)
    }
}

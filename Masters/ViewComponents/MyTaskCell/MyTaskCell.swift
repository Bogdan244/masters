//
//  MyTaskCell.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import MapKit

protocol MyTaskCellDelegate: class {
    func didSelectAcceptTaskButton(cell: MyTaskCell)
    func didSelectRemoveTaskButton(cell: MyTaskCell)
    func didSelectLocationButton(cell: MyTaskCell)
}

class MyTaskCell: UITableViewCell {

    weak var delegate: MyTaskCellDelegate?
    
    @IBOutlet weak var taskNumber: UILabel!
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var taskType: UILabel!
    @IBOutlet weak var desiredDate: UILabel!
    @IBOutlet weak var refusedComment: UILabel!
    @IBOutlet weak var unit: UILabel!
    
    @IBOutlet weak var removeTaskBT: UIButton!
    @IBOutlet weak var acceptTaskBT: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        removeTaskBT.addCornerLine()
        acceptTaskBT.addCornerLine()
    }

    func configurate(with task:TaskEntity) {
        taskNumber.attributedText = String.createText(boldText: "Номер заявки: ", nonBoldText: task.taskCode)
        address.attributedText = String.createText(boldText: "Адрес: ", nonBoldText: task.clientAddress)
        taskType.attributedText = String.createText(boldText: "Вид заявки: ", nonBoldText: task.type)
        desiredDate.attributedText = String.createText(boldText: "Желаемая дата: ", nonBoldText: task.desiredClientTime)
        refusedComment.attributedText = String.createTextIfExist(boldText: "Комментарий отказа: ", nonBoldText: task.refusedComment)
        unit.attributedText = String.createTextIfExist(boldText: "Магазин: ", nonBoldText: task.unit)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func removeTaskAction(_ sender: Any) {
        delegate?.didSelectRemoveTaskButton(cell: self)
    }
    
    @IBAction func acceptTaskAction(_ sender: Any) {
        delegate?.didSelectAcceptTaskButton(cell: self)
    }
    
    @IBAction func locationButton(_ sender: Any) {
        delegate?.didSelectLocationButton(cell: self)
    }
}

//
//  CalendarCell.swift
//  Masters
//
//  Created by admin on 11/7/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarCell: JTAppleCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    let selectColor = UIColor(red: 0.3, green: 0.3, blue: 0.3, alpha: 1)
    let deselectColor = UIColor(red: 0.60, green: 0.60, blue: 0.60, alpha: 1)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        defaultState()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        defaultState()
    }
    
    private func defaultState() {
        cellView.backgroundColor = deselectColor
        cellView.layer.cornerRadius = 7
        dayLabel.textColor = .white
        dateLabel.textColor = .white
    }
    
    func configurate(cellState: CellState) {
        let forrmater = DateFormatter()
        forrmater.dateFormat = "dd MM yyyy"
        dayLabel.text = getDayString(day: cellState.day)
        dateLabel.text = forrmater.string(from: cellState.date)
        handleState(state: cellState)
    }
    
    private func getDayString(day: DaysOfWeek) -> String {
        switch day {
        case .monday:
            return "ПН"
        case .tuesday:
            return "ВТ"
        case .wednesday:
            return "СР"
        case .thursday:
            return "ЧТ"
        case .friday:
            return "ПТ"
        case .saturday:
            return "СБ"
        case .sunday:
            return "ВС"
        }
    }
    
    func handleState(state: CellState) {
        if state.isSelected {
            cellView.backgroundColor = selectColor
        } else {
            cellView.backgroundColor = deselectColor
        }
    }
    
}

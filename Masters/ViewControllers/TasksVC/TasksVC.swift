//
//  ProposalsVC.swift
//  Masters
//
//  Created by admin on 10/17/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class TasksVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()
    var dataSource = [TaskEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurateTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadTasks()
    }
    
    fileprivate func loadTasks() {
        self.showLoader()
        TasksService.shared.getOpenAndRefusedTasks(complition: { (tasks) in
            self.refreshControl.endRefreshing()
            self.hideLoader()
            self.dataSource = tasks.tasks
            self.tableView.reloadData()
            self.tableView.removeEmptyView()
            let tabBarController = self.tabBarController as? MainTabBarVC
            tabBarController?.updateBadge(statusesCount: tasks.statusesCount)
            if tasks.tasks.count == 0 {
                self.tableView.addEmptyView(text: Constants.noTasks)
            }
            
        }) {
            self.hideLoader()
        }
    }
    
    private func configurateTableView() {
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = .white 
        tableView.addSubview(refreshControl)
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let cell = UINib.init(nibName: "TaskCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: String(describing: TaskCell.self))
    }
    
    @objc func refresh() {
        refreshControl.beginRefreshing()
        loadTasks()
    }
    
}

extension TasksVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TaskCell.self)) as! TaskCell
        cell.configurate(with: dataSource[indexPath.row])
        cell.delegate = self
        return cell
    }
    
}

extension TasksVC: TaskCellDelegate {
    
    func didSelectLocationButton(cell: TaskCell) {
        guard let index = tableView.indexPath(for: cell) else { return }
        let vc = MapVC.create(address: dataSource[index.row].clientAddress)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func didSelectButton(cell: TaskCell) {
        guard let index = tableView.indexPath(for: cell) else { return }
        TasksService.shared.acceptTask(id: dataSource[index.row].id, complition: { (task) in
            self.showAlertWithMessage(title: nil, message: task.message, complition: {
                self.loadTasks()
            })
        }) {
            self.showError()
        }
    }

}

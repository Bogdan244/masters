//
//  RefusedDepartureTaskVC.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class RefusedDepartureTaskVC: BaseVC {

    @IBOutlet weak var taskNumber: UILabel!
    @IBOutlet weak var dateDepartureLb: UILabel!
    @IBOutlet weak var timeDepartureLb: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var taskType: UILabel!
    @IBOutlet weak var desiredDate: UILabel!
    @IBOutlet weak var clientNameLb: UILabel!
    @IBOutlet weak var clientContactNumber: UILabel!
    @IBOutlet weak var unitLb: UILabel!
    @IBOutlet weak var refusedCommentLb: UILabel!
    @IBOutlet weak var commentTV: UITextView!
    @IBOutlet weak var refusedTaskBT: UIButton!
    @IBOutlet weak var copyClientPhoneBT: CopyTextButton!
    
    var handler: (() -> Void)?
    var task: TaskEntity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refusedTaskBT.addCornerLine()
        commentTV.addCorner()
        clientContactNumber.addLongTapCall()
        configurate()
    }
    
    static func create(with task: TaskEntity, handler: @escaping () -> Void) -> RefusedDepartureTaskVC {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "RefusedDepartureTaskVC") as! RefusedDepartureTaskVC
        vc.handler = handler
        vc.task = task
        return vc
    }
    
    func configurate() {
        guard let task = self.task else { return }
        
        if let dateString = task.agreedWorkingTime {
            let dateArray = dateString.components(separatedBy: " ")
            dateDepartureLb.attributedText = String.createText(boldText: "Дата выезда: ", nonBoldText: dateArray.first!)
            timeDepartureLb.attributedText = String.createText(boldText: "Время визита: ", nonBoldText: dateArray.last!)
        }
        taskNumber.attributedText = String.createText(boldText: "Номер заявки: ", nonBoldText: task.taskCode)
        address.attributedText = String.createText(boldText: "Адрес: ", nonBoldText: task.clientAddress)
        taskType.attributedText = String.createText(boldText: "Вид заявки: ", nonBoldText: task.type)
        desiredDate.attributedText = String.createText(boldText: "Желаемая дата: ", nonBoldText: task.desiredClientTime)
        clientNameLb.attributedText = String.createText(boldText: "ФИО клиента: ", nonBoldText: task.clientFio)
        clientContactNumber.attributedText = String.createText(boldText: "Контактный номер: ", nonBoldText: task.clientPhone)
        copyClientPhoneBT.setCopyText(text: task.clientPhone, for: UIControl.Event.touchUpInside)
        refusedCommentLb.attributedText = String.createTextIfExist(boldText: "Комментарий отказа: ", nonBoldText: task.refusedComment)
        unitLb.attributedText = String.createTextIfExist(boldText: "Магазин: ", nonBoldText: task.unit)
    }
    
    @IBAction func refusedAction(_ sender: Any) {
        self.showLoader()
        TasksService.shared.refusedTask(id: task!.id, comment: commentTV.text!, complition: { [weak self] (response) in
            self?.hideLoader()
            self?.showAlertWithMessage(title: nil, message: response.message, complition: {
                self?.handler?()
                self?.navigationController?.popToRootViewController(animated: true)
            })
        }) {
            self.showError()
        }
    }
    
    @IBAction func showLocationAction(_ sender: Any) {
        let vc = MapVC.create(address: (task?.clientAddress)!)
        navigationController?.pushViewController(vc, animated: true)
    }
}

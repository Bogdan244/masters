//
//  DepartureTaskVC.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import JTAppleCalendar

class DepartureTaskVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    
    let refreshControl = UIRefreshControl()
    
    var dataSource = [TaskEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurateTableView()
        configurateCalendar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadTasks()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    fileprivate func loadTasks() {
        showLoader()
        TasksService.shared.getWorkingTask(complition: { (tasks) in
            self.refreshControl.endRefreshing()
            self.hideLoader()
            self.dataSource = tasks.tasks.sorted{ $0.agreedWorkingTime ?? "" > $1.agreedWorkingTime ?? "" }
            self.tableView.reloadData()
            self.tableView.removeEmptyView()
            let tabBarController = self.navigationController?.tabBarController as? MainTabBarVC
            tabBarController?.updateBadge(statusesCount: tasks.statusesCount)
            if tasks.tasks.count == 0 {
                self.tableView.addEmptyView(text: Constants.noTasks)
            }
        }) {
            self.hideLoader()
        }
    }
    
    private func configurateTableView() {
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = .white
        tableView.addSubview(refreshControl)
        
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let cell = UINib.init(nibName: "DepartureTaskCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: String(describing: DepartureTaskCell.self))
    }
    
    @objc func refresh() {
        loadTasks()
    }
    
    private func configurateCalendar() {
        calendarView.selectDates([Date()])
        calendarView.scrollToDate(Date())
    }
    
}

extension DepartureTaskVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DepartureTaskCell.self)) as! DepartureTaskCell
        cell.configurate(with: dataSource[indexPath.row])
        cell.delegate = self
        
        return cell
    }
    
}

extension DepartureTaskVC: DepartureTaskCellDelegate {
    
    func didSelectLocationButton(cell: DepartureTaskCell) {
        guard let index = tableView.indexPath(for: cell) else { return }
        let vc = MapVC.create(address: dataSource[index.row].clientAddress)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func didSelectAcceptTaskButton(cell: DepartureTaskCell) {
        guard let index = tableView.indexPath(for: cell) else { return }
        let vc = CloseTaskVC.create(with: dataSource[index.row]) { [weak self] in
            self?.loadTasks()
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func didSelectRemoveTaskButton(cell: DepartureTaskCell) {
        guard let index = tableView.indexPath(for: cell) else { return }
        let vc = RefusedDepartureTaskVC.create(with: dataSource[index.row]) { [weak self] in
            self?.loadTasks()
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension DepartureTaskVC: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let startDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())!
        let endDate = Calendar.current.date(byAdding: .year, value: +1, to: Date())!
        let parameters = ConfigurationParameters(startDate: startDate, endDate: endDate, numberOfRows: 1, firstDayOfWeek: .monday)
        
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CalendarCell", for: indexPath) as! CalendarCell
        cell.configurate(cellState: cellState)
        
        return cell
    }

    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        guard let unCell = cell as? CalendarCell else { return }
        unCell.handleState(state: cellState)
    }

    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        guard let unCell = cell as? CalendarCell else { return }
        unCell.handleState(state: cellState)
    }
    
}

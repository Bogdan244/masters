//
//  ClosedTaskVC.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class CloseTaskVC: BaseVC {

    @IBOutlet weak var taskNumber: UILabel!
    @IBOutlet weak var dateDepartureLb: UILabel!
    @IBOutlet weak var timeDepartureLb: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var taskType: UILabel!
    @IBOutlet weak var desiredDate: UILabel!
    @IBOutlet weak var clientNameLb: UILabel!
    @IBOutlet weak var clientContactNumber: UILabel!
    @IBOutlet weak var unitLb: UILabel!
    @IBOutlet weak var refusedCommentLb: UILabel!
    @IBOutlet weak var timeTF: UITextField!
    @IBOutlet weak var commentTV: UITextView!
    @IBOutlet weak var acceptTaskBT: UIButton!
    @IBOutlet weak var copyClientPhoneBT: CopyTextButton!
    
    var task: TaskEntity?
    var handler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        acceptTaskBT.addCornerLine()
        commentTV.addCorner()
        timeTF.isHidden = true
        commentTV.isHidden = true
        clientContactNumber.addLongTapCall()
        configurate()
    }
    
    static func create(with task: TaskEntity, handler: @escaping () -> Void) -> CloseTaskVC {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "CloseTaskVC") as! CloseTaskVC
        vc.handler = handler
        vc.task = task
        return vc
    }
    
    func configurate() {
        guard let task = self.task else { return }
        
        if let dateString = task.agreedWorkingTime {
            let dateArray = dateString.components(separatedBy: " ")
            dateDepartureLb.attributedText = String.createText(boldText: "Дата выезда: ", nonBoldText: dateArray.first!)
            timeDepartureLb.attributedText = String.createText(boldText: "Время визита: ", nonBoldText: dateArray.last!)
        }
        taskNumber.attributedText = String.createText(boldText: "Номер заявки: ", nonBoldText: task.taskCode)
        address.attributedText = String.createText(boldText: "Адрес: ", nonBoldText: task.clientAddress)
        taskType.attributedText = String.createText(boldText: "Вид заявки: ", nonBoldText: task.type)
        desiredDate.attributedText = String.createText(boldText: "Желаемая дата: ", nonBoldText: task.desiredClientTime)
        clientNameLb.attributedText = String.createText(boldText: "ФИО клиента: ", nonBoldText: task.clientFio)
        clientContactNumber.attributedText = String.createText(boldText: "Контактный номер: ", nonBoldText: task.clientPhone)
        copyClientPhoneBT.setCopyText(text: task.clientPhone, for: UIControl.Event.touchUpInside)
        refusedCommentLb.attributedText = String.createTextIfExist(boldText: "Комментарий отказа: ", nonBoldText: task.refusedComment)
        unitLb.attributedText = String.createTextIfExist(boldText: "Магазин: ", nonBoldText: task.unit)
    }
    
    @IBAction func acceptTaskAction(_ sender: Any) {
        showLoader()
        TasksService.shared.closeTask(id: task!.id, comment: commentTV.text, workTime: timeTF.text!, complition: { [weak self] (response) in
            self?.hideLoader()
            self?.showAlertWithMessage(title: nil, message: response.message, complition: {
                self?.handler?()
                self?.navigationController?.popToRootViewController(animated: true)
            })
        }) {
            self.showError()
        }
    }
    
    @IBAction func showLocationAction(_ sender: Any) {
        let vc = MapVC.create(address: (task?.clientAddress)!)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension CloseTaskVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        datePicker.addTarget(self, action: #selector(valueChange), for: UIControl.Event.valueChanged)
        datePicker.locale = Locale(identifier: "ru")
        textField.inputView = datePicker
        textField.tintColor = UIColor.clear
    }
    
    @objc func valueChange(sender: UIDatePicker){
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        timeTF.text = df.string(from: sender.date)
    }
}

//
//  MainTabBarVC.swift
//  Masters
//
//  Created by admin on 10/17/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class MainTabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    static func create() -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "MainTabBarVC")
    }

    func updateBadge(statusesCount: StatusesCount) {
        tabBar.items?[0].badgeValue = String(statusesCount.open + statusesCount.refused)
        tabBar.items?[1].badgeValue = String(statusesCount.accepted)
        tabBar.items?[2].badgeValue = String(statusesCount.working)
        tabBar.items?[3].badgeValue = String(statusesCount.closed)
    }
    
}

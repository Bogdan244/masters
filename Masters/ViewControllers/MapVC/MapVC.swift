//
//  MapVC.swift
//  Masters
//
//  Created by admin on 11/5/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import GooglePlaces

class MapVC: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    var address = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GeocodeService.geodecodeAddress(address: address, complition: { [unowned self] (geodecodeEntity) in
            self.setMarker(lat: geodecodeEntity.results[0].geometry.location.lat,
                           lon: geodecodeEntity.results[0].geometry.location.lng,
                           snippet: geodecodeEntity.results[0].formattedAddress)
        }) {
            self.showAlertMessage(title: "Ошибка", message: "Не удается найти адрес")
        }
    }

    static func create(address: String) -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "MapVC") as! MapVC
        vc.address = address
        return vc
    }
    
    let zoomLevel: Float = 15
    
    private func setMarker(lat: Double, lon: Double, snippet: String) {
        let position = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let marker = GMSMarker(position: position)
        marker.snippet = snippet
        marker.map = mapView
        let camera = GMSCameraPosition.camera(withLatitude: lat,
                                              longitude: lon,
                                              zoom: zoomLevel)
        mapView.animate(to: camera)
    }
    
}

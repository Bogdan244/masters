//
//  ClosedTasksVC.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class ClosedTasksVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dataSource = [TaskEntity]()
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configurateTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadTasks()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    fileprivate func loadTasks() {
        self.showLoader()
        TasksService.shared.getClosedTask(complition: { (tasks) in
            self.hideLoader()
            self.refreshControl.endRefreshing()
            self.dataSource = tasks.tasks.sorted{ $0.workingTime ?? "" > $1.workingTime ?? "" }
            self.tableView.reloadData()
            self.tableView.removeEmptyView()
            let tabBarController = self.tabBarController as? MainTabBarVC
            tabBarController?.updateBadge(statusesCount: tasks.statusesCount)
            if tasks.tasks.count == 0 {
                self.tableView.addEmptyView(text: Constants.noClosedTasks)
            }
        }) {
            self.hideLoader()
        }
    }
    
    private func configurateTableView() {
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let cell = UINib.init(nibName: "ClosedTaskCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: String(describing: ClosedTaskCell.self))
    }

    @objc func refresh() {
        loadTasks()
    }
}

extension ClosedTasksVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ClosedTaskCell.self)) as! ClosedTaskCell
        cell.configurate(with: dataSource[indexPath.row])
        cell.delegate = self
        return cell
    }
    
}

extension ClosedTasksVC: ClosedTaskCellDelegate {
    func didSelectLocationButton(cell: ClosedTaskCell) {
        guard let index = tableView.indexPath(for: cell) else { return }
        let vc = MapVC.create(address: dataSource[index.row].clientAddress)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

//
//  MyTasksVC.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class MyTasksVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()
    var dataSource = [TaskEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurateTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadTasks()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    fileprivate func loadTasks() {
        showLoader()
        TasksService.shared.getAcceptedTasks(complition: { (tasks) in
            self.refreshControl.endRefreshing()
            self.hideLoader()
            self.dataSource = tasks.tasks.sorted{ $0.desiredClientTime > $1.desiredClientTime }
            self.tableView.reloadData()
            self.tableView.removeEmptyView()
            let tabBarController = self.navigationController?.tabBarController as? MainTabBarVC
            tabBarController?.updateBadge(statusesCount: tasks.statusesCount)
            if tasks.tasks.count == 0 {
                self.tableView.addEmptyView(text: Constants.noTasks)
            }
        }) {
            self.hideLoader()
        }
    }
    
    private func configurateTableView() {
        
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        refreshControl.tintColor = .white 
        tableView.addSubview(refreshControl)
        
        tableView.tableFooterView = UIView()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let cell = UINib.init(nibName: "MyTaskCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: String(describing: MyTaskCell.self))
    }

    @objc func refresh() {
        loadTasks()
    }
    
}

extension MyTasksVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MyTaskCell.self)) as! MyTaskCell
        cell.configurate(with: dataSource[indexPath.row])
        cell.delegate = self
        return cell
    }
    
}

extension MyTasksVC: MyTaskCellDelegate {
    func didSelectLocationButton(cell: MyTaskCell) {
        guard let index = tableView.indexPath(for: cell) else { return }
        let vc = MapVC.create(address: dataSource[index.row].clientAddress)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func didSelectAcceptTaskButton(cell: MyTaskCell) {
        guard let index = tableView.indexPath(for: cell) else { return }
        let vc = WorkingTaskVC.create(with: dataSource[index.row]) { [weak self] in
            self?.loadTasks()
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func didSelectRemoveTaskButton(cell: MyTaskCell) {
        guard let index = tableView.indexPath(for: cell) else { return }
        let vc = RefusedTaskVC.create(with: dataSource[index.row]) { [weak self] in
            self?.loadTasks()
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
   
}

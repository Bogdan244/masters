//
//  WorkingTaskVC.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class WorkingTaskVC: BaseVC {

    @IBOutlet weak var taskNumber: UILabel!
    @IBOutlet weak var addressLb: UILabel!
    
    @IBOutlet weak var desiredDateLb: UILabel!
    @IBOutlet weak var clientNameLb: UILabel!
    @IBOutlet weak var clientPhoneLb: UILabel!
    @IBOutlet weak var clientDeviceLb: UILabel!
    @IBOutlet weak var unitLb: UILabel!
    @IBOutlet weak var refusedCommentLb: UILabel!
    @IBOutlet weak var comentTv: UITextView!
    @IBOutlet weak var workingBt: UIButton!
    @IBOutlet weak var timeTF: UITextField!
    @IBOutlet weak var copyClientPhoneBT: CopyTextButton!
    
    var task: TaskEntity?
    var handler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        workingBt.addCornerLine()
        comentTv.addCorner()
        clientPhoneLb.addLongTapCall()
        configurate()
    }
    
    static func create(with task: TaskEntity, handler: @escaping () -> Void) -> WorkingTaskVC {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "WorkingTaskVC") as! WorkingTaskVC
        vc.task = task
        vc.handler = handler
        return vc
    }
    
    func configurate() {
        guard let task = self.task else { return }
        taskNumber.attributedText = String.createText(boldText: "Номер заявки: ", nonBoldText: task.taskCode)
        addressLb.attributedText = String.createText(boldText: "Адрес: ", nonBoldText: task.clientAddress)
        
        desiredDateLb.attributedText = String.createText(boldText: "Желаемая дата: ", nonBoldText: task.desiredClientTime)
        clientNameLb.attributedText = String.createText(boldText: "ФИО клиента: ", nonBoldText: task.clientFio)
        clientPhoneLb.attributedText = String.createText(boldText: "Контактный номер: ", nonBoldText: task.clientPhone)
        copyClientPhoneBT.setCopyText(text: task.clientPhone, for: UIControl.Event.touchUpInside)
        clientDeviceLb.attributedText = String.createText(boldText: "Вид заявки: ", nonBoldText: task.type)
        refusedCommentLb.attributedText = String.createTextIfExist(boldText: "Комментарий отказа: ", nonBoldText: task.refusedComment)
        unitLb.attributedText = String.createTextIfExist(boldText: "Магазин: ", nonBoldText: task.unit)
    }
    
    @IBAction func workingBTAction(_ sender: Any) {
        showLoader()
        TasksService.shared.acceptedToWork(id: task!.id, comment: comentTv.text, workTime: timeTF.text!, complition: { [weak self] (response) in
            self?.hideLoader()
            self?.showAlertWithMessage(title: nil, message: response.message, complition: {
                self?.handler?()
                self?.navigationController?.popToRootViewController(animated: true)
            })
        }) {
            self.showError()
        }
    }
    
    @IBAction func showLocationAction(_ sender: Any) {
        let vc = MapVC.create(address: (task?.clientAddress)!)
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension WorkingTaskVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        datePicker.addTarget(self, action: #selector(valueChange), for: UIControl.Event.valueChanged)
       // datePicker.setTextColor(color: UIColor.white)
        datePicker.locale = Locale(identifier: "ru")
        textField.inputView = datePicker
       // textField.inputView?.backgroundColor = UIColor(red: 83/255, green: 125/255, blue: 200/255, alpha: 1)
        textField.tintColor = UIColor.clear
        }
        
        @objc func valueChange(sender: UIDatePicker){
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
            timeTF.text = df.string(from: sender.date)
        }
}

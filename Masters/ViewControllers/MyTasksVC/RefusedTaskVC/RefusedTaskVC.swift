//
//  RefusedTaskVC.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class RefusedTaskVC: BaseVC {

    @IBOutlet weak var taskNumber: UILabel!
    @IBOutlet weak var addressLb: UILabel!
    @IBOutlet weak var desiredDateLb: UILabel!
    @IBOutlet weak var clientNameLb: UILabel!
    @IBOutlet weak var clientPhoneLb: UILabel!
    @IBOutlet weak var clientDeviceLb: UILabel!

    @IBOutlet weak var unitLb: UILabel!
    @IBOutlet weak var refusedCommentLb: UILabel!
    
    @IBOutlet weak var comentTv: UITextView!
    @IBOutlet weak var refusedBt: UIButton!
    @IBOutlet weak var copyClientPhoneBT: CopyTextButton!
    
    var task: TaskEntity?
    var handler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refusedBt.addCornerLine()
        comentTv.addCorner()
        clientPhoneLb.addLongTapCall()
        configurate()
    }
    
    static func create(with task: TaskEntity, handler: (() -> Void)?) -> RefusedTaskVC {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "RefusedTaskVC") as! RefusedTaskVC
        vc.task = task
        vc.handler = handler
        return vc
    }
    
    func configurate() {
        guard let task = self.task else { return }
        taskNumber.attributedText = String.createText(boldText: "Номер заявки: ", nonBoldText: task.taskCode)
        
        addressLb.attributedText = String.createText(boldText: "Адрес: ", nonBoldText: task.clientAddress)
        
        desiredDateLb.attributedText = String.createText(boldText: "Желаемая дата: ", nonBoldText: task.desiredClientTime)
        clientNameLb.attributedText = String.createText(boldText: "ФИО клиента: ", nonBoldText: task.clientFio)
        clientPhoneLb.attributedText = String.createText(boldText: "Контактный номер: ", nonBoldText: task.clientPhone)
        copyClientPhoneBT.setCopyText(text: task.clientPhone, for: UIControl.Event.touchUpInside)
        clientDeviceLb.attributedText = String.createText(boldText: "Вид заявки: ", nonBoldText: task.type)
        refusedCommentLb.attributedText = String.createTextIfExist(boldText: "Комментарий отказа: ", nonBoldText: task.refusedComment)
        unitLb.attributedText = String.createTextIfExist(boldText: "Магазин: ", nonBoldText: task.unit)
    }
    
    @IBAction func refusedTaskAction(_ sender: Any) {
        guard let task = self.task else { return }
        showLoader()
        TasksService.shared.refusedTask(id: task.id, comment: comentTv.text, complition: { [weak self] (response) in
            self?.hideLoader()
            self?.showAlertWithMessage(title: nil, message: response.message, complition: {
                self?.handler?()
                self?.navigationController?.popToRootViewController(animated: true)
            })
        }) {
            self.showError()
        }
    }
    
    @IBAction func showLocationAction(_ sender: Any) {
        let vc = MapVC.create(address: (task?.clientAddress)!)
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

//
//  ViewController.swift
//  Masters
//
//  Created by admin on 10/17/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var loginTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginTF.text = "380000000000"
        passwordTF.text = "123123"
    }

    @IBAction func loginAction(_ sender: Any) {
        showLoader()
        APIManager.login(phone: loginTF.text!.replacingOccurrences(of: "+", with: ""), password: passwordTF.text!).request(complition: { (response) in
            self.hideLoader()
            do {
                let user = try response.map(User.self)
                PersistanceService.shared.update(object: user)
                self.present(MainTabBarVC.create(), animated: true, completion: {
                    
                })
            } catch {
                do {
                    let item = try response.map(ResponseEntity.self)
                    self.showAlertMessage(title: nil, message: item.message)
                } catch {
                    
                }
            }
        }) { (error) in
            self.hideLoader()
            print(error)
        }
    }
    
}


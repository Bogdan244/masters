//
//  Constans.swift
//  Masters
//
//  Created by admin on 10/17/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

class Constants {
    static let userKey = "user"
    //AIzaSyBGzSA3vb1PFZsC8EmYayz1eIutm-y9UPM
    //AIzaSyCAh8E7vlZ8okbRpvZEQAM5-V0wAJ_ykw0
    static let googleMapKey = "AIzaSyCAh8E7vlZ8okbRpvZEQAM5-V0wAJ_ykw0"
    
    static let noTasks = "На данный момент нет принятых заявок."
    static let noClosedTasks = "На данный момент нет выполненных заявок."
}

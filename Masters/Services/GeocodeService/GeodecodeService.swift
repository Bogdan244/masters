//
//  GeodecodeService.swift
//  Masters
//
//  Created by admin on 11/6/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

class GeocodeService {
    
    static let regionBelarus = "BY"
    static let language = "ru"
    
    static func geodecodeAddress(address: String, complition: @escaping (GeocodeEntity) -> Void, failure: @escaping () -> Void) {
        APIManager.geodecoding(address: address,
                               region: GeocodeService.regionBelarus,
                               language: GeocodeService.language)
            .request(complition: { (response) in
                do {
                    let geodecode = try response.map(GeocodeEntity.self)
                    complition(geodecode)
                } catch {
                    print(error)
                    failure()
                }
            }) { (error) in
                failure()
            }
        }   
    
}

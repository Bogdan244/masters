//
//  TasksService.swift
//  Masters
//
//  Created by admin on 10/18/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

class TasksService {
    
    typealias complition = (_ task: TasksEntity) -> Void
    typealias complitionResponse = (_ response: ResponseEntity) -> Void
    typealias failure = () -> Void
    
    private init() {}
    static let shared = TasksService()
    
    func getTasks(status: Status, count: Int = 1000, complition: complition?, failure: failure?) {
        APIManager.list(status: status, count: count).request(complition: { (response) in
            do {
                let item = try response.map(TasksEntity.self)
                complition?(item)
            } catch {
                failure?()
            }
        }) { (error) in
            failure?()
        }
    }
    
    func getOpenTasks(complition: complition?, failure: failure?) {
        getTasks(status: .open, complition: complition, failure: failure)
    }
    func getOpenAndRefusedTasks(complition: complition?, failure: failure?) {
        getTasks(status: .openAndRefused, complition: complition, failure: failure)
    }
    
    func getAcceptedTasks(complition: complition?, failure: failure?) {
        getTasks(status: .accepted, complition: complition, failure: failure)
    }
    
    func getWorkingTask(complition: complition?, failure: failure?) {
        getTasks(status: .working, complition: complition, failure: failure)
    }
    
    func getClosedTask(complition: complition?, failure: failure?)  {
        getTasks(status: .closed, complition: complition, failure: failure)
    }
    
    func editTask(id: Int, status: Status, commentType: CommentType? = nil, comment: String? = nil, workingTimeType: WorkingTimeType? = nil, workingTime: String? = nil, complition: complitionResponse?, failure: failure?) {
        APIManager.edit(id: id, status: status, commentType: commentType, comment: comment, workingTimeType: workingTimeType, workingTime: workingTime).request(complition: { (response) in
            do {
                let item = try response.map(ResponseEntity.self)
                complition?(item)
            } catch {
                failure?()
            }
        }) { (error) in
            failure?()
        }
    }
    
    func acceptTask(id: Int, complition: complitionResponse?, failure: failure?) {
        editTask(id: id, status: .accepted, complition: complition, failure: failure)
    }
    
    func refusedTask(id: Int, comment: String, complition: complitionResponse?, failure: failure?) {
        editTask(id: id,
                 status: .refused,
                 commentType: CommentType.refusedComment,
                 comment: comment,
                 complition: complition,
                 failure: failure)
    }
    
    func acceptedToWork(id: Int, comment: String, workTime: String, complition: complitionResponse?, failure: failure?) {
        editTask(id: id,
                 status: .working,
                 commentType: CommentType.workingComment,
                 comment: comment,
                 workingTimeType: WorkingTimeType.agreedWorkingTime,
                 workingTime: workTime,
                 complition: complition,
                 failure: failure)
    }
    
    func closeTask(id: Int, comment: String, workTime: String, complition: complitionResponse?, failure: failure?) {
        editTask(id: id,
                 status: .closed,
                 commentType: CommentType.closeComment,
                 comment: comment,
                 workingTimeType: WorkingTimeType.workingTime,
                 workingTime: workTime,
                 complition: complition,
                 failure: failure)
    }
}

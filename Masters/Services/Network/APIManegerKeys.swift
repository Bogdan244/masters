//
//  APIManegerKeys.swift
//  StruggleOfMinds
//
//  Created by admin on 8/30/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

class APIManagerKey {
    
    static let baseURL = "https://masterdev.so24.net/api/"
    static let geodecodingURL = "https://maps.googleapis.com/maps/api/geocode/"
    
    static let client_id = "client_id"
    static let client_secret = "client_secret"
    static let otp = "otp"
    static let phone = "phone"
    static let password = "password"
    static let status = "status"
    static let id = "id"
    static let refusedComment = "refused_comment"
    static let workingComment = "working_planning_comment"
    static let closeComment = "close_comment"
    static let address = "address"
    static let key = "key"
    static let region = "region"
    static let language = "language"
    static let count = "count"
}

public enum CommentType: String {
    case refusedComment = "refused_comment"
    case workingComment = "working_planning_comment"
    case closeComment = "close_comment"
}

public enum WorkingTimeType: String {
    case agreedWorkingTime = "agreed_working_time"
    case workingTime = "working_time"
}

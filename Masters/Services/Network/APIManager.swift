//
//  APIManager.swift
//  StruggleOfMinds
//
//  Created by admin on 8/30/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

import Moya

public enum RangeType: String {
    case day
    case week
    case month
    case year
    case all
}

private func JSONResponseDataFormatter(data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data as Data, options: [])
        let prettyData = try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData as Data
    } catch {
        return data // fallback to original data if it cant be serialized
    }
}

// MARK: - Provider support
private extension String {
    var URLEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }
}

public enum APIManager {
    case login(phone: String, password: String)
    case info
    case logOut
    case list(status: Status, count: Int)
    case edit(id: Int, status: Status, commentType: CommentType?, comment: String?, workingTimeType: WorkingTimeType?, workingTime: String?)
    case geodecoding(address: String, region: String, language: String)
}

extension APIManager: TargetType {
    
    public var headers: [String : String]? {
        return ["Authorization" : User.current?.token ?? ""]
    }
    
    /// The method used for parameter encoding.
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    public var baseURL: URL {
        switch self {
        case .geodecoding:
            return URL(string: APIManagerKey.geodecodingURL)!
        default:
            return URL(string: APIManagerKey.baseURL)!
        }
    
    }
    
    public var method: Moya.Method {
        switch self {
        case .login(_,_), .list(_), .edit(_, _, _, _, _, _):
            return .post
        default:
            return .get
        }
    }
    
    public var path: String {
        switch self {
        case .login(_,_):
            return "auth/login_password"
        case .list(_):
            return "task/list"
        case .edit(_, _, _, _, _, _):
            return "task/edit"
        case .geodecoding(_, _, _):
            return "json"
        default:
            return ""
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    var parameters: [String : Any] {
        var value = [String: Any]()
        
        switch self {
        case .login(let phone, let password):
            value[APIManagerKey.phone] = phone
            value[APIManagerKey.password] = password
        case .list(let status, let count):
            value[APIManagerKey.status] = status
            value[APIManagerKey.count] = count
        case .edit(let id, let status, let commentType, let comment, let workingType, let workingTime):
            value[APIManagerKey.id] = id
            value[APIManagerKey.status] = status
            if let commentTypeValue = commentType { value[commentTypeValue.rawValue] = comment }
            if let workingTypeValue = workingType { value[workingTypeValue.rawValue] = workingTime }
        case .geodecoding(let address, let region, let language):
            value[APIManagerKey.address] = address
            value[APIManagerKey.region] = region
            value[APIManagerKey.language] = language
            value[APIManagerKey.key] = Constants.googleMapKey
        default:
            break
        }
        
        value[APIManagerKey.client_id] = "3"
        value[APIManagerKey.client_secret] = "1UFMSMke8Bcri73T46sTWuBWeJWU96BEWgivW4FW"
        
        return value
    }
    
    var multipartBody: [MultipartFormData] {
        let multipartBody = [MultipartFormData]()
        switch self {
    
        default:
            break
        }
        return multipartBody
    }
    
    public var task: Moya.Task {
        switch self {
        default:
            return Moya.Task.requestParameters(parameters: self.parameters, encoding: self.parameterEncoding)
        }
        
    }
    
}

extension APIManager {
    
    func request(complition: @escaping (Response) -> Void, failure: @escaping (Error) -> Void ) {
        MoyaProvider<APIManager>(endpointClosure: endpointClousure, plugins: plugins).request(self) { (result) in
            switch result {
            case .success(let respone):
                complition(respone)
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    private var endpointClousure: (TargetType) -> Endpoint {
        func endpointClousure(target: TargetType) -> Endpoint {
            return Endpoint(url: self.baseURL.appendingPathComponent(self.path).absoluteString,
                            sampleResponseClosure: { .networkResponse(200, self.sampleData) },
                            method: self.method,
                            task: self.task,
                            httpHeaderFields: self.headers)
        }
        
        return endpointClousure
    }
    
    private var plugins: [PluginType] {
        return [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)]
    }
    
}

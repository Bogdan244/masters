//
//  PersistanceService.swift
//  StruggleOfMinds
//
//  Created by admin on 8/31/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import RealmSwift

class PersistanceService  {
    
    private init() {}
    static let shared = PersistanceService()
    
    var realm = try! Realm()
    
    func create<T: Object>(object: T) {
        do {
            try realm.write {
                realm.add(object)
            }
        } catch {
            print(error)
        }
    }
    
    func update<T: Object>(object: T) {
        do {
            try realm.write {
                realm.add(object, update: true)
            }
        } catch {
            print(error)
        }
    }
    
}
